var map = L.map('map').setView([19.8454, -90.5237], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

var customIcon = new L.Icon({
  iconUrl: 'img/beer.png',
  iconSize: [50, 50],
  iconAnchor: [25, 50]
});
var oxxos =[
    {
        "id": 1,
        "name": "Oxxo Concordia Campeche",
        "colony": {
            "id": 6,
            "name": "Col. Mexico"
        },
        "lat": "19.8342089",
        "lng": "-90.5008639",
        "allday": true,
        "beer": false,
        "parking": true
    },
    {
        "id": 2,
        "name": "OXXO Independencia",
        "colony": {
            "id": 4,
            "name": "Centro"
        },
        "lat": "19.8462909",
        "lng": "-90.5365785",
        "allday": true,
        "beer": false,
        "parking": false
    },
    {
        "id": 3,
        "name": "OXXO PITAHAYA",
        "colony": {
            "id": 1,
            "name": "Santa Ana"
        },
        "lat": "19.83028054",
        "lng": "-90.53123434",
        "allday": true,
        "beer": false,
        "parking": true
    },
    {
        "id": 4,
        "name": "OXXO 3 PODERES",
        "colony": {
            "id": 1,
            "name": "Santa Ana"
        },
        "lat": "19.8359038",
        "lng": "-90.5220144",
        "allday": true,
        "beer": true,
        "parking": true
    },
    {
        "id": 5,
        "name": "OXXO Casa De Justicia",
        "colony": {
            "id": 8,
            "name": "Las Flores"
        },
        "lat": "19.8244161",
        "lng": "-90.5268001",
        "allday": false,
        "beer": false,
        "parking": true
    },
    {
        "id": 6,
        "name": "Oxxo Fracciorama",
        "colony": {
            "id": 13,
            "name": "Fracciorama 2000"
        },
        "lat": "19.828423",
        "lng": "-90.5347222",
        "allday": true,
        "beer": false,
        "parking": true
    },
    {
        "id": 7,
        "name": "OXXO CATASTRO",
        "colony": {
            "id": 4,
            "name": "Centro"
        },
        "lat": "19.8487524",
        "lng": "-90.5358306",
        "allday": false,
        "beer": false,
        "parking": true
    },
    {
        "id": 8,
        "name": "Oxxo San Rafael",
        "colony": {
            "id": 15,
            "name": "San Rafael"
        },
        "lat": "19.8137996",
        "lng": "-90.5294478",
        "allday": true,
        "beer": true,
        "parking": true
    },
    {
        "id": 9,
        "name": "OXXO Calle 10",
        "colony": {
            "id": 4,
            "name": "Centro"
        },
        "lat": "19.845972",
        "lng": "-90.53671",
        "allday": false,
        "beer": false,
        "parking": false
    },
    {
        "id": 10,
        "name": "OXXO Resurgimiento",
        "colony": {
            "id": 19,
            "name": "Bosques de Campeche"
        },
        "lat": "19.8318888",
        "lng": "-90.5586464",
        "allday": true,
        "beer": true,
        "parking": true
    },
    {
        "id": 11,
        "name": "Oxxo Instituto Campechano",
        "colony": {
            "id": 4,
            "name": "Centro"
        },
        "lat": "19.8438171",
        "lng": "-90.5403808",
        "allday": true,
        "beer": false,
        "parking": false
    },
    {
        "id": 12,
        "name": "Oxxo Cooperativa Kalá",
        "colony": {
            "id": 16,
            "name": "Kala"
        },
        "lat": "19.8549731",
        "lng": "-90.4805955",
        "allday": true,
        "beer": true,
        "parking": true
    },
    {
        "id": 13,
        "name": "oxxo Lerma",
        "colony": {
            "id": 25,
            "name": "Lerma"
        },
        "lat": "19.8110012",
        "lng": "-90.5940725",
        "allday": true,
        "beer": true,
        "parking": true
    }
];
//cada iteración esta tomando un array con latitud, longitud y lugar
oxxos.forEach(function(elemento){
	L.marker([elemento.lat, elemento.lng], {icon:customIcon} ).addTo(map)
	.openPopup();
});

